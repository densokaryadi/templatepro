<?php
// print_r($_SESSION);
$success="";    
if (isset ($_POST['submit'])){
    $username	=  htmlentities($_POST['login'], ENT_QUOTES, "UTF-8");
	$password		=  htmlentities($_POST['login-password'], ENT_QUOTES, "UTF-8");
    $ceklogin = $Mdishrisclass->ceklogin($username,$password);
    if($ceklogin['success']=="1")
    {
        $msg =$ceklogin['msg'];
        $infoalert ="success";
        $success="OK";
        $kata = "Success Login";
        // print_r($ceklogin);
        // echo "DENI".$ceklogin['data']['cabang'];
        $Mdishrisclass->simpansessionhris($ceklogin['data']);
        $Mdismoclass->simpansessionmdismo($ceklogin['data']['cabang']);
       
                    
        header("Refresh:3; url=content.php?menu=dashboard");
    }
    else{

        $msg =$ceklogin['msg'];
        $infoalert ="danger";
        $success="OK";
        $kata = "Gagal Login";
    }  
           
        

}
 include 'inc/config.php';
 include 'inc/template_start.php'; 
?>

<!-- Login Full Background -->
<!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
<img src="img/placeholders/backgrounds/login_full_bg.jpg" alt="Login Full Background" class="full-bg animation-pulseSlow">
<!-- END Login Full Background -->

<!-- Login Container -->
<div id="login-container" class="animation-fadeIn">
    <!-- Login Title -->
    <div class="login-title text-center">
        <h1>  <strong>MDISMO Inventory Barcode System</strong><br><small>Please <strong>Login</strong>  </small></h1>
    </div>
    <!-- END Login Title -->
    
    <!-- Login Block -->
    <div class="block push-bit">
        <?php 
                if(isset($_POST))
                {
                    if($success=="OK")
                    {
                        ?>
                        <div class="alert alert-<?php echo $infoalert;?>" no-border mb-2" role="alert">
                                <strong><?php echo $kata;?></strong> <?php echo $msg;?>.
                        </div>
                        <?php
                    }
                }
        ?>
        <!-- Login Form -->
        <form action="#" method="post" id="form-login" class="form-horizontal form-bordered form-control-borderless">
        <input type="hidden" name="submit" id="submit" value="submit">
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        <input type="text" id="login" name="login" class="form-control input-lg" placeholder="User MDIS">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                        <input type="password" id="login-password" name="login-password" class="form-control input-lg" placeholder="Password">
                    </div>
                </div>
            </div>
               <div class="form-group form-actions">
			 
                <div class="col-xs-4 text-right">
                    <button type="submit" name="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Dashboard</button>
                </div>  
				
				<div class="col-xs-12 text-right">
                   <a href="<?php echo $gethost;?>" class="btn btn-sm btn-warning"><i class="fa fa-angle-left"></i> Back to home</a>
                </div>
            </div>
			 
        </form>
       
    </div>
    <!-- END Login Block -->
</div>
<!-- END Login Container -->

<!-- Modal Terms -->
 
<!-- END Modal Terms -->

<?php include 'inc/template_scripts.php'; ?>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/login.js"></script>
<script>$(function(){ Login.init(); });</script>

<?php include 'inc/template_end.php'; ?>
