<?php

class Mdisportalclass extends db
{
    // public function Mdisportalclass(){
    function __construct(){
		$clArgs = func_get_args();
		if(count($clArgs)>0){
			$this->db = $clArgs[0];
		}
	}
    public function cekLogin($username,$password)
    {
        $sqlcek = "select * from user where username='".$username."' and password ='".$password."'";

        $getCekLogin = $this->db->query($sqlcek);
        $row = $this->db->fetchArray($getCekLogin);
        return $row;
    }
    public function simpansession($arrlogin)
    {
        $_SESSION['username']			 = $arrlogin['username'];
        $_SESSION['cabangmo']			 = $arrlogin['cabang'];
        $_SESSION['nama']			 = $arrlogin['nama'];
        $_SESSION['copyright']			 = $arrlogin['nama'];
        $_SESSION['id_login'] = $arrlogin['id_user'];

        $_SESSION['useridmo']    	= $arrlogin['username'];
        $_SESSION['levelmo']    	= $arrlogin['level_user'];
        $userid					 	= $arrlogin['username'];
        $nama						= $arrlogin['nama'];
        $cab 						= $arrlogin['cabang'];
        $tgl					 	= date('Y-m-d');
        $_SESSION['namastaffmo']   	= $nama 	; 
        
        date_default_timezone_set('Asia/Jakarta');
        $arrContextOptions=array(
            "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
            ),
        ); 
        return true;
    }

    public function getListUser($id_user)
    {
        $addwhere="";
        if($id_user!="")
        {
            $addwhere = " where id_user='".$id_user."'";

        }
        $sql = "select id_user,username,password,nama,kode_profil,cabang,server,areaid,ip,email,level_user,flag_hris,status,user_create,date_create,user_modif,date_modif
        from user ".$addwhere." ";
        echo $sql;
        $getMenu = $this->db->query($sql);
        if($id_user!="")
        {
            $array = $this->db->fetchArray($getMenu);

        }
        else {
            $array = $this->db->fetchAll($getMenu);
        }
        
        
        return $array;
    }
    public function getListUserName($username)
    {
        $addwhere="";
        if($username!="")
        {
            $addwhere = " where username='".$username."'";

        }
        $sql = "select id_user,username,password,nama,kode_profil,cabang,server,areaid,ip,email,level_user,flag_hris,status,user_create,date_create,user_modif,date_modif
        from user ".$addwhere." ";
        // echo $sql;
        $getMenu = $this->db->query($sql);
        if($username!="")
        {
            $array = $this->db->fetchArray($getMenu);

        }
        else {
            $array = $this->db->fetchAll($getMenu);
        }
        
        
        return $array;
    }
    public function getListUserAplikasi($id_profil)
    {
        $addwhere="";
        if($id_profil!="")
        {
            $addwhere = " where kode_profil like '%".$id_profil."%'";

        }
        $sql = "select id_user,username,password,nama,kode_profil,cabang,server,areaid,ip,status,user_create,date_create,user_modif,date_modif
        from user ".$addwhere." ";
        // echo $sql;
        $getMenu = $this->db->query($sql);
        $array = $this->db->fetchAll($getMenu);
        
        return $array;
    }

    public function getMenu($id_app)
    {
        $sql = "select m.id_menu,m.id_app,app.nama_app,m.nama_menu,m.ket_menu,m.icon_menu,m.url_menu,m.target_menu,m.status,m.user_create,m.date_create,m.user_modif,m.date_modif 
        from menu  m join aplikasi app on m.id_app=app.id_app 
        where m.id_app='".$id_app."' order by urutan asc
        ";
        // echo $sql;
        $getMenu = $this->db->query($sql);
        $array = $this->db->fetchAll($getMenu);
        return $array;
    }
    public function getMenuSPM($id_app)
    {
        // print_r($_SESSION);
        $sql = "select m.id_menu,m.id_app,app.nama_app,m.nama_menu,m.ket_menu,m.icon_menu,m.url_menu,m.target_menu,m.status,m.user_create,m.date_create,m.user_modif,m.date_modif 
        from menu  m inner join aplikasi app on m.id_app=app.id_app 
        inner join cfg_menu_profil cmp on cmp.id_menu=m.id_menu 
        inner join cfg_app_profil cap on cap.id_app=app.id_app and cap.id_profil=cmp.id_profil
        inner join profil prf on prf.id_profil=cap.id_profil     
        where m.id_app='".$id_app."' and 
        prf.id_profil in (select kode_profil from user where username='".$_SESSION['username']."') 
        order by urutan asc
        ";
        // echo $sql;
        $getMenu = $this->db->query($sql);
        $array = $this->db->fetchAll($getMenu);
        return $array;
    }
    public function getSubMenu($id_menu)
    {
        $sql = "select sm.id_sub_menu,
        sm.id_menu,
        
        sm.nama_submenu,
        sm.ket_submenu,
        m.nama_menu,
        m.ket_menu,
        sm.icon_submenu,
        sm.url_submenu,
        sm.target_submenu,
        sm.user_create,
        sm.date_create,
        sm.user_modif,sm.date_modif 
        from sub_menu sm
        join menu m on sm.id_menu=m.id_menu 
        where sm.id_menu='".$id_menu."'
        ";
        // echo $sql;
        $getSubMenu = $this->db->query($sql);
        $array = $this->db->fetchAll($getSubMenu);
        return $array;
    }
    public function getListSubMenu($id_app,$id_menu){
        $sql = "select sm.id_sub_menu,
        sm.id_menu,
        sm.nama_submenu,
        ap.nama_app,
        ap.id_app,
        sm.ket_submenu,
        m.nama_menu,
        m.ket_menu,
        sm.icon_submenu,
        sm.status,
        sm.url_submenu,
        sm.target_submenu,
        sm.user_create,
        sm.date_create,
        sm.user_modif,sm.date_modif 
        from sub_menu sm
        join menu m on sm.id_menu=m.id_menu 
        join aplikasi ap on ap.id_app=m.id_app
        where sm.id_menu='".$id_menu."' and ap.id_app='".$id_app."'
        ";
        // echo $sql;
        $getSubMenu = $this->db->query($sql);
        $array = $this->db->fetchAll($getSubMenu);
        return $array;
    }
    public function getSubMenuInfo($id_app,$id_menu,$id_submenu){
        $sql = "select sm.id_sub_menu,
        sm.id_menu,
        sm.nama_submenu,
        ap.nama_app,
        ap.id_app,
        sm.ket_submenu,
        m.nama_menu,
        m.ket_menu,
        sm.icon_submenu,
        sm.status,
        sm.url_submenu,
        sm.target_submenu,
        sm.user_create,
        sm.date_create,
        sm.user_modif,sm.date_modif 
        from sub_menu sm
        join menu m on sm.id_menu=m.id_menu 
        join aplikasi ap on ap.id_app=m.id_app
        where sm.id_menu='".$id_menu."' and ap.id_app='".$id_app."' and sm.id_sub_menu='".$id_submenu."'
        ";
        // echo $sql;
        $getSubMenu = $this->db->query($sql);
        $array = $this->db->fetchAll($getSubMenu);
        return $array;
    }
    public function getMenuInfo($id_menu)
    {
        $sql = "select m.id_app,
        m.id_menu,
        m.nama_menu,
        m.ket_menu,
        m.icon_menu,
        m.url_menu,
        m.target_menu,
        m.status,
        m.user_create,
        m.date_create,
        m.user_modif,m.date_modif 
        from menu m
        where m.id_menu='".$id_menu."'
        ";
        // echo $sql;
        $getSubMenu = $this->db->query($sql);
        $array = $this->db->fetchAll($getSubMenu);
        return $array;
    }
    
    public function insertupdateprofil($tipe,$id_profil,$kode_profil,$nama_profil){
        if($tipe=="insert"){
            $insupd = "insert into profil (kode_profil,nama_profil,user_create,date_create,user_modif,date_modif)
            values ('".$kode_profil."','".$nama_profil."','admin',current_timestamp(),'admin',current_timestamp())
            ";
        }
        else{
            $insupd ="update profil set kode_profil='".$kode_profil."',nama_profil='".$nama_profil."'
            ,user_modif='admin',date_modif=current_timestamp() where id_profil='".$id_profil."'";
        }
        // echo $insupd;
        $insert = $this->db->query($insupd);
        return $insert;
    }
    public function insertupdatedataaplikasi($tipe,$id_app,$judul_app,$nama_app,$footer_app,$template_app,$author_app,$link_app){
        if($tipe=="insert"){
            $insupd = "insert into aplikasi (judul_app,nama_app,footer_app,template,author,link,user_create,date_create,user_modif,date_modif)
            values ('".$judul_app."','".$nama_app."','".$footer_app."','".$template_app."','".$author_app."','".$link_app."','admin',current_timestamp(),'admin',current_timestamp())
            ";
        }
        else{
            $insupd ="update aplikasi set judul_app='".$judul_app."',nama_app='".$nama_app."',footer_app='".$footer_app."',template='".$template_app."',
                author='".$author_app."',link='".$link_app."',user_modif='admin',date_modif=current_timestamp() where id_app='".$id_app."'";
        }
        // echo $insupd;
        $insert = $this->db->query($insupd);
        return $insert;
    }
    public function insertupdatedatamenu($tipe,$id_app,$id_menu_app,$nama_app,$nama_menu,$ket_menu,$url_menu,$status,$target,$icon_menu){
        if($tipe=="insert"){
            $insupd = "insert into menu (id_app,nama_menu,ket_menu,icon_menu,url_menu,status,target_menu,user_create,date_create,user_modif,date_modif)
            values ('".$id_app."','".$nama_menu."','".$ket_menu."','".$icon_menu."','".$url_menu."','".$status."','".$target."','admin',current_timestamp(),'admin',current_timestamp())
            ";
        }
        else{
            $insupd ="update menu set 
            nama_menu='".$nama_menu."',
            ket_menu='".$ket_menu."',
            icon_menu='".$icon_menu."',
            url_menu='".$url_menu."',
            status='".$status."',
            target_menu='".$target."',
            user_modif='admin',
            date_modif=current_timestamp()
            where id_menu='".$id_menu_app."'
            ";

            // $insupd ="update aplikasi set judul_app='".$judul_app."',nama_app='".$nama_app."',footer_app='".$footer_app."',template='".$template_app."',
                // author='".$author_app."',link='".$link_app."',user_modif='admin',date_modif=current_timestamp() where id_app='".$id_app."'";
        }
        // echo $insupd;
        $insert = $this->db->query($insupd);
        return $insert;
    }
    public function insertupdatedatasubmenu($tipe,$id_menu_app,$id_submenu_app,$nama_app,$nama_submenu,$ket_submenu,$url_submenu,$status,$target,$icon_submenu){
        if($tipe=="insert"){
            $insupd = "insert into sub_menu (id_menu,nama_submenu,ket_submenu,icon_submenu,url_submenu,status,target_submenu,user_create,date_create,user_modif,date_modif)
            values ('".$id_menu_app."','".$nama_submenu."','".$ket_submenu."','".$icon_submenu."','".$url_submenu."','".$status."','".$target."','admin',current_timestamp(),'admin',current_timestamp())
            ";
        }
        else{
            $insupd ="update sub_menu set 
            nama_submenu='".$nama_submenu."',
            ket_submenu='".$ket_submenu."',
            icon_submenu='".$icon_submenu."',
            url_submenu='".$url_submenu."',
            status='".$status."',
            target_submenu='".$target."',
            user_modif='admin',
            date_modif=current_timestamp()
            where id_sub_menu='".$id_submenu_app."'
            ";

            // $insupd ="update aplikasi set judul_app='".$judul_app."',nama_app='".$nama_app."',footer_app='".$footer_app."',template='".$template_app."',
                // author='".$author_app."',link='".$link_app."',user_modif='admin',date_modif=current_timestamp() where id_app='".$id_app."'";
        }
        // echo $insupd;
        $insert = $this->db->query($insupd);
        return $insert;
    }
    
    public function checkuseravailable($username)
    {
        $addwhere="";
        if($username!="")
        {
            $addwhere = " where lower(username) ='".strtolower($username)."'";
        }
        $sql = "select * from user ".$addwhere."";
        // echo $sql;
        $check = $this->db->query($sql);
        $jml = $this->db->numRows($check);
        return $jml;
    }
    public function updateinsertuser($tipe,$username,$password,$nama,$imp_kode_profil,$cabang,$id_user,$flag_hris,$email,$level){
        if($tipe=="insert"){
            $insert  = "
            
            insert into user (username,password,nama,kode_profil,cabang,status,level_user,email,flag_hris,user_create,date_create,user_modif,date_modif) values 
            
            ('".$username."','".$password."','".$nama."','".$imp_kode_profil."','".$cabang."','1','".$level."','".$email."','".$flag_hris."','admin',current_timestamp(),'admin',current_timestamp())
            ";
        }
        else{
            $insert ="
                    update user set password='".$password."',email='".$email."',flag_hris='".$flag_hris."',level_user='".$level."',nama='".$nama."',kode_profil='".$imp_kode_profil."', cabang='".$cabang."' where id_user='".$id_user."'
            ";
        }
        // echo $insert;
        $insert = $this->db->query($insert);
        return $insert ;
    }
    public function getDataAplikasi($id_app)
    {
        $addwhere="";
        if($id_app!="")
        {
            $addwhere = " where id_app ='".$id_app."'";
        }
        $sql = "select id_app,judul_app,nama_app,footer_app,template,author,link,user_create,date_create,user_modif,date_modif from aplikasi ".$addwhere."";
        // echo $sql;
        $getSubMenu = $this->db->query($sql);
        $array = $this->db->fetchAll($getSubMenu);
        return $array;
    }
    public function getDataProfile($id_profil)
    {
        $addwhere="";
        if($id_profil!="")
        {
            $addwhere = " where id_profil ='".$id_profil."'";
        }
        $sql = "select id_profil,kode_profil,nama_profil,user_create,date_create,user_modif,date_modif from profil ".$addwhere." order by kode_profil asc ";
        // echo $sql;
        $getSubMenu = $this->db->query($sql);
        if($id_profil!="")
        {
            $array = $this->db->fetchArray($getSubMenu);
        }
        else{
            $array = $this->db->fetchAll($getSubMenu);
        }
        
        return $array;
    }
    public function deleteallappmenusubmenu($id_profil){
        $deleteapp = "delete from cfg_app_profil where id_profil='".$id_profil."' ";
        $delete1 = $this->db->query($deleteapp);
        $deletemenu = "delete from cfg_menu_profil where id_profil='".$id_profil."'  ";
        $delete2 = $this->db->query($deletemenu);
        $deletesubmenu = "delete from cfg_submenu_profil where id_profil='".$id_profil."'";
        $delete3 = $this->db->query($deletesubmenu);
        return true;
    }
    public function deleteaplikasi($id_app){
        $sql = "delete from aplikasi where id_app='".$id_app."'";
        // echo $sql;
        $delete = $this->db->query($sql);
        return $delete;
    }
    public function deletemenu($id_app,$id_menu){
        $sql = "delete from menu where id_app='".$id_app."' and id_menu='".$id_menu."'";
        // echo $sql;
        $delete = $this->db->query($sql);
        return $delete;
    }
    public function deletesubmenu($id_app,$id_menu,$id_submenu){
        $sql = "delete from sub_menu where id_menu='".$id_menu."' and id_sub_menu='".$id_submenu."'";
        // echo $sql;
        $delete = $this->db->query($sql);
        return $delete;
    }
    
    public function deleteprofile($id_profil){
        $sql = "delete from profil where id_profil='".$id_profil."'";
        // echo $sql;
        $delete = $this->db->query($sql);
        return $delete;
    }
    public function getCheckedCfgApp($id_profil,$id_app){
        $sql = "select * from cfg_app_profil where id_profil='".$id_profil."' and id_app='".$id_app."'";
        
        $check = $this->db->query($sql);
        $jml = $this->db->numRows($check);
        return $jml;
    }
    public function getCheckedCfgMenu($id_profil,$id_menu){
        $sql = "select * from cfg_menu_profil where id_profil='".$id_profil."' and id_menu='".$id_menu."'";
        // echo $sql."<br>";
        $check = $this->db->query($sql);
        $jml = $this->db->numRows($check);
        return $jml;
    }
    public function getCheckedCfgSubMenu($id_profil,$id_submenu){
        $sql = "select * from cfg_submenu_profil where id_profil='".$id_profil."' and id_submenu='".$id_submenu."'";
        
        $check = $this->db->query($sql);
        $jml = $this->db->numRows($check);
        return $jml;
    }
    
    public function getAppProfile($id_profil)
    {
        $addwhere="";
        if($id_profil!="")
        {
            $addwhere = " where cf.id_profil in(".$id_profil.")";
        }
        $sql = "select cf.id_profil,cf.id_app,cf.kode_profil,cf.nama_profil,cf.nama_app,app.judul_app,app.author,app.link,app.template,app.footer_app
        from cfg_app_profil cf inner join aplikasi app on app.id_app=cf.id_app  ".$addwhere."";
        // echo $sql;
        $getSubMenu = $this->db->query($sql);
        $array = $this->db->fetchAll($getSubMenu);
        
        
        return $array;
    }
    public function deleteinsertcfgapp($id_profil,$kode_profil,$nama_profil,$__id_app,$__nama_app){
        $delete = "delete from cfg_app_profil where id_profil='".$id_profil."' and id_app='".$__id_app."'";
        $delete = $this->db->query($delete);
        if($delete){
            $insert  = "insert into cfg_app_profil (id_profil,id_app,kode_profil,nama_profil,nama_app,user_create,date_create,user_modif,date_modif) values 
            
            ('".$id_profil."','".$__id_app."','".$kode_profil."','".$nama_profil."','".$__nama_app."','admin',current_timestamp(),'admin',current_timestamp())
            ";
            $insert = $this->db->query($insert);
        }
        return $insert ;
    }
    public function deleteinsertcfgmenu($id_profil,$kode_profil,$nama_profil,$_id_menu,$_nama_menu){
        $delete = "delete from cfg_menu_profil where id_profil='".$id_profil."' and id_menu='".$_id_menu."'";
        $delete = $this->db->query($delete);
        if($delete){
            $insert  = "insert into cfg_menu_profil (id_profil,id_menu,kode_profil,nama_profil,nama_menu,user_create,date_create,user_modif,date_modif) values 
            
            ('".$id_profil."','".$_id_menu."','".$kode_profil."','".$nama_profil."','".$_nama_menu."','admin',current_timestamp(),'admin',current_timestamp())
            ";
            $insert = $this->db->query($insert);
        }
        return $insert ;

    }
    public function deleteinsertcfgsubmenu($id_profil,$kode_profil,$nama_profil,$_id_submenu,$_nama_submenu){
        $delete = "delete from cfg_submenu_profil where id_profil='".$id_profil."' and id_submenu='".$_id_submenu."'";
        $delete = $this->db->query($delete);
        if($delete){
            $insert  = "insert into cfg_submenu_profil (id_profil,id_submenu,kode_profil,nama_profil,nama_submenu,user_create,date_create,user_modif,date_modif) values 
            
            ('".$id_profil."','".$_id_submenu."','".$kode_profil."','".$nama_profil."','".$_nama_submenu."','admin',current_timestamp(),'admin',current_timestamp())
            ";
            $insert = $this->db->query($insert);
        }
        return $insert ;

    }
    public function GenerateKodeProfil()
    {
        $sql = "SELECT CONCAT('A',LPAD(jml,4,'0')) as kode_profil from ( select count(*)+1 as jml from profil ) as a";
        $getSubMenu = $this->db->query($sql);
        $array = $this->db->fetchArray($getSubMenu);    
        return $array; 
    }
    
    
}

?>
