<?php

class Mdisspmclass extends db
{
    // public function Mdisportalclass(){
    function __construct(){
		$clArgs = func_get_args();
		if(count($clArgs)>0){
			$this->db = $clArgs[0];
		}
	}
    public function getDataKategoriID($id_kat)
    {
        $addwhere =" where id_kategori='".$id_kat."'";
        $sqlcek = "select * from kategori ".$addwhere." order by kode asc";

        $getCekLogin = $this->db->query($sqlcek);
        $array = $this->db->fetchArray($getCekLogin);
        return $array;
    }
    public function getDataKategoriKode($kode_kat)
    {
        $addwhere =" where kode='".$kode_kat."'";
        $sqlcek = "select * from kategori ".$addwhere." order by kode asc";

        $getCekLogin = $this->db->query($sqlcek);
        $array = $this->db->fetchArray($getCekLogin);
        return $array;
    }
    public function getListDataKategori($tipe)
    {
        $addwhere ="";
        if($tipe!=""){
            $addwhere =" where tipe ='".$tipe."'";
        }
        $sqlcek = "select * from kategori ".$addwhere." order by kode asc";

        $getCekLogin = $this->db->query($sqlcek);
        $array = $this->db->fetchAll($getCekLogin);
        return $array;
    }
    public function getJenisKegiatanPerBulan($jenis)
    {
        // print_r($_SESSION);
        $addwhere ="";
        if($_SESSION['levelmo']=="Admin"){
            $addwhere .= " and a.user_create='".$_SESSION['username']."'";
        }
        $sqlcek = "select b.kode,b.nama,count(*) as jml from mst_laporan a join kategori b on b.id_kategori=a.id_kategori and tipe='".$jenis."'
        ".$addwhere."
        group by b.kode,b.nama order by b.kode";

        $getCekLogin = $this->db->query($sqlcek);
        $array = $this->db->fetchAll($getCekLogin);
        return $array;
    }
    public function getKegiatanKomdevPerBulan($jenis)
    {
        $addwhere ="";
        if($_SESSION['levelmo']=="Admin"){
            $addwhere .= " and a.user_create='".$_SESSION['username']."'";
        }
        $sqlcek = "select a.user_create,count(*) as jml from mst_laporan a join kategori b on b.id_kategori=a.id_kategori and tipe='".$jenis."'
        ".$addwhere."
        group by a.user_create order by a.user_create";

        $getCekLogin = $this->db->query($sqlcek);
        $array = $this->db->fetchAll($getCekLogin);
        return $array;
    }
    public function getTotalAnggotaPenyuluhan($jenis)
    {
        $addwhere ="where 1=1";
        $addwhere .= "  ";
        if($_SESSION['levelmo']=="Admin"){
            $addwhere .= " and a.user_create='".$_SESSION['username']."'";
        }
        $sqlcek = "select b.kode,b.nama,sum(jml_peserta) as jml from mst_laporan a join kategori b on b.id_kategori=a.id_kategori and tipe='".$jenis."'
        ".$addwhere."
        group by b.kode,b.nama order by  b.kode asc";

        $getCekLogin = $this->db->query($sqlcek);
        $array = $this->db->fetchAll($getCekLogin);
        return $array;
    }
    
    public function getListLaporan($tipe)
    {
        $addwhere ="";
        
        // if($tipe!=""){
        //     $addwhere =" where tipe ='".$tipe."'";
        // }

        $sqlcek = "select * from mst_laporan ".$addwhere." order by id_laporan asc";
        $getCekLogin = $this->db->query($sqlcek);
        $array = $this->db->fetchAll($getCekLogin);
        return $array;
    }
    
    public function GenerateKodeKategori()
    {
        $sql = "SELECT CONCAT('A',LPAD(jml,4,'0')) as kode_kategori from ( select count(*)+1 as jml from kategori ) as a";
        $getSubMenu = $this->db->query($sql);
        $array = $this->db->fetchArray($getSubMenu);    
        return $array; 
    }
    public function insertupdatekategori($tipe,$kode_kategori,$nama_kategori,$status,$_tipeinsdel){
        if($_tipeinsdel=="insert"){
            $insupd = "insert into kategori (tipe,kode,nama,status,user_create,date_create,user_modif,date_modif)
            values ('".$tipe."','".$kode_kategori."','".$nama_kategori."','".$status."','admin',current_timestamp(),'admin',current_timestamp())
            ";
        }
        else{
            $insupd ="update kategori set nama='".$nama_kategori."',status='".$status."'
            ,user_modif='admin',date_modif=current_timestamp() where kode='".$kode_kategori."'";
        }
        // echo $insupd;
        $insert = $this->db->query($insupd);
        return $insert;
    }
    public function insupddatalaporan($id_kategori,
    $kode_kategori,$tgl_dari,$tgl_sampai,$waktu_dari,$waktu_sampai,$list_cabang,$judul,$center,$keterangan,$anggota,$jml_peserta,$pathfoto
    ,$pathfile,$id_laporan,$file_before,$image_before,$id_jenistema){
        // echo strlen(trim($id_laporan));
       if (strlen(trim($id_laporan))>0){
           $insupd ="
            update mst_laporan set tgl_dari='".$tgl_dari."',tgl_sampai='".$tgl_sampai."',jam_sampai='".$waktu_dari."',jam_dari='".$waktu_sampai."',
            cabang='".$list_cabang."',center='".$center."',judul='".$judul."',keterangan='".$keterangan."',anggota='".$anggota."',
            jml_peserta='".$jml_peserta."',foto='".$image_before."',dokumen='".$file_before."',
            user_modif='".$_SESSION['useridmo']."',date_modif=current_timestamp(),id_jenistema='".$id_jenistema."',status='1'
            where id_laporan='".$id_laporan."'
           ";
       }
       else{
       
        // if($_tipeinsdel=="insert"){
            $insupd = "insert into mst_laporan (id_kategori,tgl_dari,tgl_sampai,jam_dari,jam_sampai,cabang,center,judul,keterangan,anggota,jml_peserta,foto,dokumen,status,
            user_create,date_create,user_modif,date_modif,id_jenistema)
            values (
                '".$id_kategori."','".$tgl_dari."','".$tgl_sampai."','".$waktu_dari."',
                '".$waktu_sampai."','".$list_cabang."','".$center."','".$judul."',
                '".$keterangan."','".$anggota."','".$jml_peserta."','".$pathfoto."','".$pathfile."'
                ,'1','".$_SESSION['useridmo']."',current_timestamp(),'".$_SESSION['useridmo']."',current_timestamp(),'".$id_jenistema."')
            ";
            // echo $insupd;
        }
        // else{
            // $insupd ="update kategori set nama='".$nama_kategori."',status='".$status."'
            // ,user_modif='admin',date_modif=current_timestamp() where kode='".$kode_kategori."'";
        // }
        // echo $insupd;
        $insert = $this->db->query($insupd);
        return $insert;
    }
    
    public function getDataLaporanSPMById($id_lap){
        $addwhere="where 1=1 ";
        if($id_lap!=""){
            $addwhere .= " and id_laporan ='".$id_lap."'";
        }
        if($_SESSION['levelmo']=="Admin"){
            $addwhere .= " and a.user_create='".$_SESSION['username']."'";
        }
        
        $sqllaporanspm = " select
        DATE_FORMAT(CAST(a.date_create AS DATE) ,'%d/%m/%Y') as tgl_input,
        a.id_laporan,
        a.id_kategori,
        b.kode,
        b.nama,
        c.kode as jenis_tema,
        c.nama as nama_jenis_tema,
        date_format(a.tgl_dari,'%d/%m/%Y') as tgl_dari,
        date_format(a.tgl_sampai,'%d/%m/%Y') as tgl_sampai,
        a.jam_dari,
        a.jam_sampai,
        a.cabang,
        a.center,
        a.judul,
        a.keterangan,
        a.anggota,
        a.alasan,
        a.jml_peserta,
        a.foto,
        a.dokumen,
        a.status,
        a.user_create,
        a.date_create,
        a.user_modif,
        a.date_modif
        from mst_laporan a 
        left join kategori b on a.id_kategori = b.id_kategori  and b.tipe='TEMA'
        left join kategori c on a.id_jenistema = c.id_kategori  and c.tipe='JENISTEMA'
        ".$addwhere."
        order by a.date_create desc
        ";
        $getDataLaporanSpm = $this->db->query($sqllaporanspm);
        if($id_lap!=""){
        $array = $this->db->fetchArray($getDataLaporanSpm);
        }
        else{
            $array = $this->db->fetchAll($getDataLaporanSpm);
        }
        return $array;

    }
    public function getDataLaporanSPMByMonth($date){
        $addwhere="where 1=1 ";
        if($date!=""){
            // $addwhere = " and DATE_FORMAT(CAST(a.date_create AS DATE) ,'%Y-%m')='".$date."'";
            $addwhere .= " and ('".$date."' between date_format(a.tgl_dari,'%Y-%m') and date_format(a.tgl_sampai,'%Y-%m'))";
        }
        if($_SESSION['levelmo']=="Admin"){
            $addwhere .= " and a.user_create='".$_SESSION['username']."'";
        }
        
        $sqllaporanspm = " select
        DATE_FORMAT(CAST(a.date_create AS DATE) ,'%d/%m/%Y') as tgl_input,
        a.id_laporan,
        a.id_kategori,
        a.id_jenistema,
        b.kode,
        b.nama,
        c.kode as jenis_tema,
        c.nama as nama_jenis_tema,
        date_format(a.tgl_dari,'%d/%m/%Y') as tgl_dari,
        date_format(a.tgl_sampai,'%d/%m/%Y') as tgl_sampai,
        a.jam_dari,
        a.jam_sampai,
        a.cabang,
        a.center,
        a.judul,
        a.keterangan,
        a.anggota,
        a.alasan,
        a.jml_peserta,
        a.foto,
        a.dokumen,
        a.status,
        a.user_create,
        a.date_create,
        a.user_modif,
        a.date_modif
        from mst_laporan a 
        left join kategori b on a.id_kategori = b.id_kategori  and b.tipe='TEMA'
        left join kategori c on a.id_jenistema = c.id_kategori  and c.tipe='JENISTEMA'
        ".$addwhere."
        ";
        // echo $sqllaporanspm;
        $getDataLaporanSpm = $this->db->query($sqllaporanspm);
        // if($id_lap!=""){
        // $array = $this->db->fetchArray($getDataLaporanSpm);
        // }
        // else{
            $array = $this->db->fetchAll($getDataLaporanSpm);
        // }
        return $array;

    }
    public function approverejectdatalaporan($id_laporan,$upd_status,$alasan){
        $update = "update mst_laporan set alasan ='".$alasan."', status='".$upd_status."',user_modif='".$_SESSION['username']."',date_modif=current_timestamp() 
        where id_laporan='".$id_laporan."'";
        $resu = $this->db->query($update);
        return $resu; 
    }
}

?>
