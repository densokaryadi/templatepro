<?php

class Mdismoapiclass 
{
    // public function Mdismoapiclass(){
    function __construct(){
		$clArgs = func_get_args();
		if(count($clArgs)>0){
			$this->api = $clArgs[0];
		}
	}
    public function getInfoUrl()
    {
        // echo $this->api;
    }
    function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();
        $arrContextOptions=array(
            "ssl"=>array(
               "verify_peer"=>false,
               "verify_peer_name"=>false,
            ),
        ); 
        
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        if($method=="GET")
        {
            $result 		= file_get_contents("$url", false, stream_context_create($arrContextOptions));
        }
        else{
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    
            $result = curl_exec($curl);
            curl_close($curl);
        }


        return $result;
    }
    public function ceklogin($username,$password)
    {
        $urlapi 	= $this->api."/api/hris/ceklogin";
        $data       = array("userid" =>$username,"passid"=>$password);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result);
        return $obj;
    }
    public function cekloginmdismo($username)
    {
        $urlapi 	= $this->api."/api/hris/cekloginmdismo";
        $data       = array("username" =>$username);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result);
        return $obj;
    }
    public function simpansession($session)
    {
        $data = $session->{'data'};
        foreach ($data as $value) 
        $userid = $value->user_id;
        $level_user = $value->level_user;
        $nama = $value->nama;
        $cabang = $value->cabang;

        $_SESSION['useridmo']    	= $userid;
        $_SESSION['levelmo']    	= $level_user;
        $userid					 	= $userid;
        $nama						= $nama;
        $_SESSION['cabangmo']		= $cabang;
        $cab 						= $cabang;
        $tgl					 	= date('Y-m-d');
        $_SESSION['namastaffmo']   	= $nama; 
    }
    public function getInfoUserMdis($srv,$cabang,$user)
    {
        $urlapi 	= $this->api."/api/mdis1/getinfousermdismo";
        // echo $urlapi;
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"user"=>$user);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getInfoRefNoStatus($srv,$cabang,$tgl,$nik)
    {
        $urlapi 	= $this->api."/api/mdis1/getinforefnostatus";
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"tgl"=>$tgl,"nik"=>$nik);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getSetoranHarian($srv,$cabang,$tgl,$centerid)
    {
        $urlapi 	= $this->api."/api/mdis1/getsetoranharian";
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"tgl"=>$tgl,"centerid"=>$centerid);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getCekMdBranch($srv,$cabang)
    {
        $urlapi 	= $this->api."/api/mdis1/getmdbranch";
        $data       = array("srv"=>$srv,"cabang"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getMdCenter($srv,$cabang)
    {
        $urlapi 	= $this->api."/api/mdis1/getmdcenter";
        $data       = array("srv"=>$srv,"cabang"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getMdCenterClient($srv,$center,$cabang)
    {
        $urlapi 	= $this->api."/api/mdis1/getmdclientcenter";
        $data       = array("srv"=>$srv,"center"=>$center,"cabang"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getMdClient($srv,$cabang)
    {
        $urlapi 	= $this->api."/api/mdis1/getmdclient";
        $data       = array("srv"=>$srv,"cabang"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getDailySaving($srv,$cabang,$tgl)
    {
        $urlapi 	= $this->api."/api/mdis1/getdailysaving";
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"tgl"=>$tgl);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getDailyLoan($srv,$cabang,$tgl)
    {
        $urlapi 	= $this->api."/api/mdis1/getdailyloan";
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"tgl"=>$tgl);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getOfficer($srv,$cabang)
    {
        $urlapi 	= $this->api."/api/mdis1/getofficer";
        $data       = array("srv"=>$srv,"cabang"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getDisburse($srv,$cabang,$tgl)
    {
        $urlapi 	= $this->api."/api/mdis1/getdisburse";
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"tgl"=>$tgl);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getNamaAdmin($srv,$cabang)
    {
        $urlapi 	= $this->api."/api/mdis1/getnamaadmin";
        $data       = array("srv"=>$srv,"cabang"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getFamilyMdis($srv)
    {
        $urlapi 	= $this->api."/api/mdis1/gettablefamily";
        $data       = array("srv"=>$srv);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getProductSavingBranchMdis($srv,$cabang)
    {
        $urlapi 	= $this->api."/api/mdis1/getproductsavingbranchmdis";
        $data       = array("srv"=>$srv,"cabang"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getTransNoMDSetoranHarian($srv,$cabang,$tgl,$center)
    {
        $urlapi 	= $this->api."/api/mdis1/gettransnomdsetoranharian";
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"tgl"=>$tgl,"center"=>$center);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getAccountNoMdSaving($srv,$cabang,$clientid)
    {
        $urlapi 	= $this->api."/api/mdis1/getaccountmdsavingaccount";
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"clientid"=>$clientid);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getCounterAccount($srv,$cabang,$kop,$nosa)
    {
        $urlapi 	= $this->api."/api/mdis1/getcounteraccount";
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"kop"=>$kop,"periode"=>$nosa);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getNisbahMdDailyLoanLedger($srv,$loanno,$date)
    {
        $urlapi 	= $this->api."/api/mdis1/getnisbahmddailyloanledger";
        $data       = array("srv"=>$srv,"loanno"=>$loanno,"tgl"=>$date);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function ExecuteGenerateTLP($srv,$cabang,$kop,$nosa,$loanno,$date,$pokok,$nisbahb,$user)
    {
        $urlapi 	= $this->api."/api/mdis1/executegeneratetlp";
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"kop"=>$kop,"nosa"=>$nosa,"loanno"=>$loanno,"date"=>$date,"pokok"=>$pokok,"nisbahb"=>$nisbahb,"user"=>$user);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    
    public function getListOfficer($srv,$cabang)
    {
        $urlapi 	= $this->api."/api/mdis1/getlistofficer";
        $data       = array("srv"=>$srv,"cabang"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getListKabupatenProvinsi($srv,$cabang)
    {
        $urlapi 	= $this->api."/api/mdis1/getinfokabupatenprovinsi";
        $data       = array("srv"=>$srv,"cabang"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getListKecamatan($srv,$cabang)
    {
        $urlapi 	= $this->api."/api/mdis1/getinfokecamatan";
        $data       = array("srv"=>$srv,"cabang"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getListDesa($srv,$cabang,$kecamatan)
    {
        $urlapi 	= $this->api."/api/mdis1/getinfodesa";
        $data       = array("srv"=>$srv,"cabang"=>$cabang,"kecamatan"=>$kecamatan);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }

    public function getInfoStaffLapangUk($kode,$cabang,$tanggal)
    {
        // echo "sini";
        $urlapi 	= $this->api."/api/mdismo/getinfostafflapang";
        $data       = array("kode"=>$kode,"cabang"=>$cabang,"tanggal"=>$tanggal);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getInfoStaffLapangHris($cabang,$nik)
    {
        // echo "sini";
        $urlapi 	= $this->api."/api/hris/getinfostafflapanghris";
        $data       = array("cabang"=>$cabang,"officer"=>$nik);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getListCenter($srv,$cabang)
    {
        
        $urlapi 	= $this->api."/api/mdis1/getlistcenter";
        $data       = array("srv"=>$srv,"BranchCode"=>$cabang);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    public function getInfoDataAnggotaSPM($srv,$id_client)
    {
        
        $urlapi 	= $this->api."/api/mdis1/getinfodataanggotaspm";
        $data       = array("srv"=>$srv,"id_client"=>$id_client);
        $result     = $this->CallAPI("GET",$urlapi,$data);
        $obj        = json_decode($result,true);
        return $obj;
    }
    
        
}

?>
