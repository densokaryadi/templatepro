<?php

class Mdismobarcodeclass extends db
{
    // public function Mdismobarcodeclass(){
    function __construct(){
		$clArgs = func_get_args();
		if(count($clArgs)>0){
			$this->db = $clArgs[0];
		}
	}
    public function setapi($api)
    {
        $this->api = $api;
    }
    public function getListBarcode(){
        $sap = "select sn as barcode from tbl_sn_tablet where not exists(select 1 from tbl_barang where barcode=sn)";
        
        $getListServer = $this->db->query($sap);
        $row = $this->db->fetchAll($getListServer);
        return $row;
    }
    
    public function getTotalTabletCabang($cabang){
        $sap = "select kd_barang,barcode,cabang,nik,staff,tgl_efektif,noasset,kondisi,ket_kondisi from tbl_barang where cabang='".$cabang."'";
        // echo $sap;
        $getInfoTablet = $this->db->query($sap);
        $row = $this->db->numRows($getInfoTablet);
        return $row;
    }
    public function getTotalKerusakan($cabang){
        $sap = "select kd_barang,barcode,cabang,nik,staff,tgl_efektif,noasset,kondisi,ket_kondisi from tbl_barang where cabang='".$cabang."' and kondisi='MASALAH'";
        // echo $sap;
        $getInfoTablet = $this->db->query($sap);
        $row = $this->db->numRows($getInfoTablet);
        return $row;
    }
    
    public function getInfoTablet($kd_barang){
        $sap = "select kd_barang,barcode,cabang,nik,staff,tgl_efektif,noasset,kondisi,ket_kondisi from tbl_barang where kd_barang='".$kd_barang."'";
        // echo $sap;
        $getInfoTablet = $this->db->query($sap);
        $row = $this->db->fetchArray($getInfoTablet);
        return $row;
    }
    public function getListTablet($cabang){
        $sap = "select kd_barang,barcode,cabang,nik,staff,tgl_efektif,noasset,kondisi,ket_kondisi from tbl_barang where cabang='".$cabang."'";
        // echo $sap;
        $getInfoTablet = $this->db->query($sap);
        $row = $this->db->fetchAll($getInfoTablet);
        return $row;
    }
    
    public function insertUpdateTablet($tipe,$nik,$namastaff,$noasset,$kondisi,$kondisi_ket,$barcode,$_cabang,$kd_barang,$tgl_efektif){
        if($tipe=='edit')
        {
            $sql = "update tbl_barang set nik='".$nik."',tgl_efektif='".$tgl_efektif."',staff='".$namastaff."',noasset='".$noasset."',kondisi='".$kondisi."',ket_kondisi='".$kondisi_ket."',
            barcode ='".$barcode."', cabang='".$_cabang."' where kd_barang='".$kd_barang."'";
        }
        else{
            $sql = "insert into tbl_barang (barcode,cabang,nik,staff,tgl_efektif,noasset,kondisi,ket_kondisi) values 
            
            ('".$barcode."','".$_cabang."','".$nik."','".$namastaff."','".$tgl_efektif."','".$noasset."','".$kondisi."','".$kondisi_ket."')
            ";
        }
        // echo $sql;
     
        $sql = $this->db->query($sql);
        return $sql;
    }
    public function deletebarcodetablet($kd_barang)
    {
        $delete = "delete from tbl_barang where kd_barang ='".$kd_barang."'";
        $sql = $this->db->query($delete);
        return $sql;
    }
}

?>
