<?php
error_reporting( E_ALL );
ini_set('display_errors', 1);
ini_set('max_execution_time', 0); // 0 = Unlimited

@session_start();
// print_r($_SESSION);
if (!isset ($_SESSION['namastaffmo'])) {
   header("location: /spm");

}
else
{
    if (!isset ($_SESSION['id_app'])) {
        header("location: /portalkomida");
     
    }
    else{
        if($_SESSION['id_app']!="9"){
            header("location: /portalkomida");
     
        }
    }   
    
    $nama	= $_SESSION['namastaffmo'];
    $cabang = $_SESSION['cabangmo'];
    $srv 	= $_SESSION['server'];
    $hari 	= date("l");
    
  
    include "db/connect.php";
    include "db/db.php";
    
    include 'inc/function.php';  
    include "class/Mdismoclass.php";
    include "class/Mdishrisclass.php";
    include "class/Mdismoapiclass.php";
    include "class/Mdismobarcodeclass.php";

    include "class/Mdisportalclass.php";
    include "class/Mdisspmclass.php";

    $dbmdismo = new db($server,$username,$password,$database);
    $dbmdismobarcode = new db($serverbr,$usernamebr,$passwordbr,$databasebr);
    $dbhris = new db($serverhr,$usernamehr,$passwordhr,$databasehr);

    $dbportal = new db($serverpor,$usernamepor,$passwordpor,$databasepor);
    $dbspm = new db($serverspm,$usernamespm,$passwordspm,$databasespm);

    $Mdismoclass=new Mdismoclass($dbmdismo);
    $Mdishrisclass=new Mdishrisclass($dbhris);
    $Mdismoapiclass=new Mdismoapiclass($link_api);
    $Mdismobarcodeclass=new Mdismobarcodeclass($dbmdismobarcode);
    $Mdisportalclass=new Mdisportalclass($dbportal);
    $Mdisspmclass =new Mdisspmclass($dbspm);
    $getInfoUrl = $Mdismoapiclass->getInfoUrl();

//     echo "sini";
//     print_r($_SESSION);
//     @session_start();
// @session_unset();
// @session_destroy();
// exit;

    // exit;
    if(!$_SESSION){
        header("/portalkomida");  
    } 
    // echo $serverbr;
    if (isset ($_POST['cabang_dashboard'])){
	 
        $Mdismoclass->simpansessionmdismo($_POST['cabang_dashboard']);
        $nama	= $_SESSION['namastaffmo'];
        $cabang = $_SESSION['cabangmo'];
        $srv 	= $_SESSION['server'];
        $hari 	= date("l");
        
                
    }
    include 'inc/config.php';  
    include 'inc/template_start.php';  
    include 'inc/page_head.php';

	$tgl	= date("Y-m-d");
	$md 	= meetingday($hari);
    $getInfoCabang =  $Mdismoclass->getInfoCabang($cabang);
    $namac = $getInfoCabang['branchname'];

?>

<!-- Page content -->
<div id="page-content">
<?php  
                   
    if(isset($_GET['menu']))
    {
        switch ($_GET['menu']) {
            case 'dashboard':
                # code...
                include "content/module/dashboard.php";
                break;
            case 'kategori':
                    include "content/module/kategori.php";
                break;
            case 'jenistema':
                include "content/module/jenistema.php";
            break;
            case 'tambahjenistema':
                include "content/module/tambahjenistema.php";
            break;
            
            case 'tambahkategori':
                include "content/module/tambahkategori.php";
            break;
            case 'tambahdatalaporan':
                include "content/module/tambahdatalaporan.php";
            break;
            case 'editdatalaporan':
                include "content/module/editdatalaporan.php";
            break;         
            case 'detaildatalaporan':
                include "content/module/detaildatalaporan.php";
            break;         
               
            case 'laporan':
                include "content/module/laporan.php";
            break;
            case 'laporanbulanan':
                include "content/module/laporanbulanan.php";
            break;
                      
            default:
                # code...
                // include "login.php";
                break;
        }
    }
?>   
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="js/Chart.bundle.js"></script>
<!-- <script src="js/pages/compCharts.js"></script> -->
<!-- <script>$(function(){ CompCharts.init(); });</script> -->
<!-- Load and execute javascript code used only in this page -->
<!-- <script src="js/pages/tablesDatatables.js"></script> -->
<!-- <script>$(function(){ TablesDatatables.init(); });</script> -->
<script src="js/allscript.js"></script>

<?php include 'inc/template_end.php'; ?>
<?php

}
?>