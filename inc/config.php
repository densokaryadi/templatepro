<?php
@session_start();
/**
 * config.php
 *
 * Author: pixelcave
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */

/* Template variables */
//$th = array( "night", "amethyst", "modern", "autumn", "flatie", "spring", "fancy", "fire", "coral", "lake", "forest", "waterlily", "emerald", "blackberry");
//$ther = array_rand($th,1);
//echo $ther['0'];
$template = array(
    'name'              => $_SESSION['judul_app'],
    'version'           => '1.1',
    'author'            => 'MIS',
    'robots'            => 'noindex, nofollow',
    'title'             => $_SESSION['judul_app'],
    'description'       => 'Secret.',
    // true                     enable page preloader
    // false                    disable page preloader
    'page_preloader'    => false,
    // true                     enable main menu auto scrolling when opening a submenu
    // false                    disable main menu auto scrolling when opening a submenu
    'menu_scroll'       => true,
    // 'navbar-default'         for a light header
    // 'navbar-inverse'         for a dark header
    'header_navbar'     => 'navbar-default',
    // ''                       empty for a static layout
    // 'navbar-fixed-top'       for a top fixed header / fixed sidebars
    // 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
    'header'            => '',
    // ''                                               for a full main and alternative sidebar hidden by default (> 991px)
    // 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
    // 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
    // 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
    // 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
    // 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
    // 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
    // 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
    // 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
    // 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
    // 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
    'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
    // ''                       empty for a static footer
    // 'footer-fixed'           for a fixed footer
    'footer'            => 'footer-fixed',
    // ''                       empty for default style
    // 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
    'main_style'        => '',
    // ''                           Disable cookies (best for setting an active color theme from the next variable)
    // 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
    'cookies'           => '',
    // 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
    // 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
    'theme'             => 'spring',
    // ''                       for default content in header
    // 'horizontal-menu'        for a horizontal menu in header
    // This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
    'header_content'    => '',
    'active_page'       => basename($_SERVER['REQUEST_URI'])
);
// print_r($_SESSION);
$getMenu = $Mdisportalclass->getMenuSPM($_SESSION['id_app']);
// echo "<pre>";
// print_r($getMenu);
// echo "</pre>";
// exit;
if(is_array($getMenu)){
    if(count($getMenu)>0){
        $response = array();
        $response1 = array();
        // asort($getMenu);
        // $i="";
        // $_getSubMenu="";
        foreach ($getMenu as $idx=>$val){
            $sub_res_menu = array();
            
            $h['name'] = $val['ket_menu'];
            $h['url'] = $val['url_menu'];
            $h['menu'] = $val['nama_menu'];
            $h['icon'] = "fa ". $val['icon_menu'];           
            $h['target'] = $val['target_menu'];
            $h['sub'] =array();
            $_getSubMenu =$Mdisportalclass->getSubMenu($val['id_menu']);
            // array_push($response["data"], $h);
               
            if(count($_getSubMenu)>0){
                // $i=0;
                foreach ($_getSubMenu as $idx1 =>$vala){
                    if(!$vala) break;
                    if($val['id_menu']==$vala['id_menu'])
                    {
                        $__idsubmenu = $vala['id_sub_menu'];
                        
                        $h['sub'][$idx1]['id_menu'] = $val['id_menu'];
                        $h['sub'][$idx1]['namamenu'] = $val['ket_menu'];
                        $h['sub'][$idx1]['id'] = $vala['id_sub_menu'];
                        $h['sub'][$idx1]['name'] = $vala['ket_submenu'];
                        $h['sub'][$idx1]['url'] = $vala['url_submenu'];
                        $h['sub'][$idx1]['menu'] = $vala['nama_submenu'];
                        $h['sub'][$idx1]['icon'] = "fa ". $vala['icon_submenu'];           
                        $h['sub'][$idx1]['target'] = $vala['target_submenu'];
                        // $response[]=$h;
                       
                    }
                    
                }
            }
            
            $response[]=$h;
        }
        
        $primary_nav = $response;
        
    }
}
// print_r( $primary_nav);

// echo "<pre>";
// print_r($primary_nav);
// echo "</pre>";
// exit;
/* Primary navigation array (the primary navigation will be created automatically based on this array, up to 3 levels deep) */

// $primary_nav = array(
//     array(
//         'name'  => 'Dashboard',
//         'url'   => 'content.php?menu=dashboard',
//         'menu'  =>'dashboard',
//         'icon'  => 'fa fa-clock-o',
//         'target' =>'_self'
//     ), 
//     array(
//         'name'  => 'Tablet List',
//         'url'   => 'content.php?menu=tablet',        
//         'menu'  =>'center',
//         'icon'  => 'gi gi-notes_2',
//         'target' =>'_self'
//     ),
//     array(
//         'name'  => 'Cetak Barcode',
//         'url'   => 'content.php?menu=cetakbarcode',        
//         'menu'  =>'center',
//         'icon'  => 'gi gi-print',
//         'target' => '_blank'
//     )   
// );