$(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
	// Kita sembunyikan dulu untuk loadingnya
	 
	$("#loadingkel").hide();
	
	 
	
	$("#center").change(function(){ // Ketika user mengganti atau memilih data provinsi
		$("#kelompok").hide(); // Sembunyikan dulu combobox kota nya
		$("#loadingkel").show(); // Tampilkan loadingnya
	
		$.ajax({
			type: "POST", // Method pengiriman data bisa dengan GET atau POST
			url: "option_center.php", // Isi dengan url/path file php yang dituju
			data: {center : $("#center").val(), cabang : $("#cabang").val()  }, // data yang akan dikirim ke file yang dituju
			dataType: "json",
			beforeSend: function(e) {
				if(e && e.overrideMimeType) {
					e.overrideMimeType("application/json;charset=UTF-8");
				}
			},
			success: function(response){ // Ketika proses pengiriman berhasil
				setTimeout(function(){
					$("#loadingkel").hide(); // Sembunyikan loadingnya

					// set isi dari combobox kota
					// lalu munculkan kembali combobox kotanya
					$("#kelompok").html(response.data_center).show();
				}, 3000);
			},
			error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
				alert(thrownError); // Munculkan alert error
			}
		});
    });
	
	
});
