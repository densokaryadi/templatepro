function test(){
    alert('test');
}
function logout(){
    $.ajax({
        type: "POST",
        url: 'action.php?act=logout',
        success: function (response) {
            var jsonData = JSON.parse(response);

            // alert(JsonData.Success);
            if (jsonData.Success) {
                swal({
                    title: "Logout!",
                    text: jsonData.Message,
                    icon: "success",
                    button: "OK!",
                });
                if(jsonData.Link=="index.php"){
                    window.location.replace("index.php");
                }
                else{
                    window.open("http://"+jsonData.Link+"/","_self");
                }
                // document.location("content.php?menu=form_uji_kelayakan&notrx="+jsonData.NoTrx+"")
            } else {
                swal({
                    title: "Logout!",
                    text: jsonData.Message,
                    icon: "warning",
                    button: "OK!",
                });
            }
        }
    });
}
function tambahdatalaporan()
{
    window.location.href = "content.php?menu=tambahdatalaporan";
}
function tambahdatakategori()
{
    window.location.href = "content.php?menu=tambahkategori";
}
function tambahdatajenistema()
{
    window.location.href = "content.php?menu=tambahjenistema";
}
function
 kembalikekategori()
{
    window.location.href = "content.php?menu=kategori";
}
function kembalikejenistema()
{
    window.location.href = "content.php?menu=jenistema";
}
function kembalikelaporan(key)
{
    // alert(key);
    if(key=="dash"){
        window.location.href = "content.php?menu=dashboard";

    }
    else{
        window.location.href = "content.php?menu=laporan";
    }
}

function kembalikedashboard()
{
    window.location.href = "content.php?menu=dashboard";
}

function submitlaporan(status,id_laporan){
    // alert(status);
    // alert(id_laporan);
    var nama="";
    if(status=="approve"){
        nama = " Apakah anda Yakin untuk Approve Data ini?";
    }
    else{
        nama = " Apakah anda Yakin untuk Reject Data ini?";
    }
    swal({
        title: nama,
        icon: "warning",
        buttons: ["Batal", "Simpan"]
        })
    .then((willSave) => {
        if(willSave)
        {
            if(status=="reject")
            {
            swal({
                text: 'Masukan Alasan di Tolak',
                content: "input",
                button: {
                  text: "Simpan!",
                  closeModal: false,
                },
              })
              .then(name => {
                if (!name) throw null;
                $.ajax({
                    type: "POST",
                    url: 'action.php?act=approverejectlaporan',
                    data:  {
                        status:status,
                        id_laporan:id_laporan,
                        alasan:name
                    },
                    // processData: false,
                    // contentType: false,
                    success: function (response) {
                        var jsonData = JSON.parse(response);

                        // alert(JsonData.Success);
                        if (jsonData.Success) {
                            swal({
                                title: "Data Confirmation!",
                                text: jsonData.Message,
                                icon: "success",
                                button: "OK!",
                            });
                            window.location.replace("content.php?menu=dashboard");
                            // document.location("content.php?menu=form_uji_kelayakan&notrx="+jsonData.NoTrx+"")
                        } else {
                            swal({
                                title: "Error Data!",
                                text: jsonData.Message,
                                icon: "warning",
                                button: "OK!",
                            });
                        }
                    }
                });
              })
              .catch(err => {
                if (err) {
                  swal("Oh noes!", "The AJAX request failed!", "error");
                } else {
                  swal.stopLoading();
                  swal.close();
                }
              });
            }
            else{
                $.ajax({
                    type: "POST",
                    url: 'action.php?act=approverejectlaporan',
                    data:  {
                        status:status,
                        id_laporan:id_laporan,
                        alasan:''
                    },
                    // processData: false,
                    // contentType: false,
                    success: function (response) {
                        var jsonData = JSON.parse(response);

                        // alert(JsonData.Success);
                        if (jsonData.Success) {
                            swal({
                                title: "Data Confirmation!",
                                text: jsonData.Message,
                                icon: "success",
                                button: "OK!",
                            });
                            window.location.replace("content.php?menu=dashboard");
                            // document.location("content.php?menu=form_uji_kelayakan&notrx="+jsonData.NoTrx+"")
                        } else {
                            swal({
                                title: "Error Data!",
                                text: jsonData.Message,
                                icon: "warning",
                                button: "OK!",
                            });
                        }
                    }
                });
            }
        }
    });
}
$(document).ready(function () {

    const month = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];

    const d = new Date();
    let name = month[d.getMonth()];
    let year = d.getFullYear();
    var _menu = $('#_form_menu').val();
    if(_menu=="dashboard"){

   
        $.ajax(
        {
            type: "POST",
            url: 'action.php?act=load_jeniskegiatan_bulan',
            data: {
                check_chart:'1',
        },
            
            success: function(response) {
            
                var jsonData = JSON.parse(response);
                new Chart(document.getElementById("chartjeniskegiatan"), {
                    type: 'bar',
                    data: {
                        labels: jsonData.label,
                        datasets: [
                        {
                            label: "Jumlah Kegiatan",
                            backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                            data: jsonData.jml
                        }
                        ]
                    },
                    options: {
                        legend: { display: false },
                        title: {
                        display: true,
                        text: 'Jenis Kegiatan Komdev pada Bulan '+name+' '+year
                        }
                    }
                });
                $('#jeniskegiatanperbulan').dataTable({
                    columnDefs: [{
                            targets: [0,1,3],
                            className: 'dt-body-center'
                        }
                    ],
                    
                    paging: false,
                    "pageLength": 10,
                    searching: false,
                    ordering: true,
                    ajax: "action.php?act=load_jeniskegiatan_bulan",
                    columns: [
                    {
                    "data": "no"
                    },
                    {
                        "data": "kode"
                    },
                    {
                    "data": "nama"
                    },
                    {
                    "data": "jumlah"
                    }
                    ]
            
                });
                // jeniskegiatan.reload();
            }
        });
        $.ajax(
            {
                type: "POST",
                url: 'action.php?act=load_kegiatankomdev_bulan',
                
                success: function(response) {
                
                    var jsonData = JSON.parse(response);
        
        
                    new Chart(document.getElementById("chartkegiatankomdev"), {
                        type: 'bar',
                        data: {
                            labels: jsonData.label,
                            datasets: [
                            {
                                label: "Total Kegiatan",
                                backgroundColor: jsonData.color,
                                data: jsonData.jml
                            }
                            ]
                        },
                        options: {
                            legend: { display: false },
                            title: {
                            display: true,
                            text: 'Kegiatan Komdev pada Bulan '+name+' '+year
                            }
                        }
                    });
                    // alert('test');
                    // c
                    $('#kegiatankomdevperbulan').dataTable({
                        columnDefs: [{
                                targets: [0,2],
                                className: 'dt-body-center'
                            }
                        ],
                        
                        paging: false,
                        "pageLength": 10,
                        searching: false,
                        ordering: true,
                        ajax: "action.php?act=load_kegiatankomdev_bulan",
                        columns: [
                        {
                        "data": "no"
                        },
                        {
                        "data": "user"
                        },
                        {
                        "data": "nama"
                        },
                        {
                        "data": "jumlah"
                        }
                        ]
                
                    });
                
                }
            });
        $.ajax(
            {
                type: "POST",
                url: 'action.php?act=load_totalanggotapenyuluhan_bulan',
                
                success: function(response) {
                
                    var jsonData = JSON.parse(response);
            
                    // alert(jsonData.jml);
                    // alert(jsonData.jml);
                    // alert(jsonData.color);
                    new Chart(document.getElementById("chartjmlanggotapenyuluhan"), {
                        type: 'pie',
                        data: {
                            labels: jsonData.label,
                            datasets: [
                            {
                                label: "Total Peserta Berdasarkan Kegiatan",
                                backgroundColor: jsonData.color,
                                data: jsonData.jml
                            }
                            ]
                        },
                        options: {
                            // legend: { display: false },
                            title: {
                            display: true,
                            text: 'Total Peserta Berdasarkan Kegiatan pada Bulan '+name+' '+year
                            }
                        }
                    });
                    // alert('test');
                    // c
                    $('#jmlanggotapenyuluhanperbulan').dataTable({
                        columnDefs: [{
                                targets: [0],
                                className: 'dt-body-center'
                            }
                        ],
                        
                        paging: false,
                        "pageLength": 10,
                        searching: false,
                        ordering: true,
                        ajax: "action.php?act=load_totalanggotapenyuluhan_bulan",
                        columns: [
                        {
                        "data": "no"
                        },
                        {
                        "data": "kodejeniskegiatan"
                        },
                        {
                        "data": "namajeniskegiatan"
                        },
                        {
                        "data": "jumlah"
                        }
                        ]
                
                    });
                
                }
            });
    }
    $("#datepickeraa").datepicker( {
        format: "yyyy-mm",
        startView: "months", 
        minViewMode: "months"
    });
    
    var _menu = $('#_form_menu').val();
    $("#loadingcenter").hide();
    $("#loadinganggota").hide();
    $("#loadingpt").hide();
    $("#loadingjp").hide();
    $("#loadingpk").hide();
    $("#loadingper").hide();
    if(_menu=="tambahdatalaporan")
    {
        // alert('test');
        hideForm("DEFAULT");
    }


    if(_menu=="editdatalaporan" || _menu=="detaildatalaporan")
    {
        // alert('test');
        var kode_kategori = $("#kode_kategori").val();
        // alert(kode_kategori);
        hideForm(kode_kategori);
    }
    
    function hideForm(kode) {
        // alert(kode);
        $("#loadingcenter").hide();
        $("#loadinganggota").hide();
        $("#loadingpt").hide();
        $("#loadingjp").hide();
        $("#loadingpk").hide();
        $("#loadingper").hide();
        if(kode=="A0001"){
            document.getElementById('formkategori').style.display='block';
            document.getElementById('formperiode').style.display='block';
            document.getElementById('formwaktu').style.display='block';
            document.getElementById('formcabang').style.display='block';
            document.getElementById('formcenter').style.display='none';
            document.getElementById('formjenistema').style.display='block';
            document.getElementById('formtema').style.display='block';
            document.getElementById('formketerangan').style.display='block';
            document.getElementById('formanggota').style.display='none';
            document.getElementById('formjmlpeserta').style.display='block';
            document.getElementById('formuploadlampiran').style.display='block';
            document.getElementById('formuploadfoto').style.display='block';
            document.getElementById('savedatalaporan').disabled =false;   
            document.getElementById('formanggotapt').style.display='none';
            document.getElementById('formanggotajp').style.display='none';
            document.getElementById('formanggotapk').style.display='none';
            document.getElementById('formanggotaer').style.display='none';         

    }else if(kode=="A0002"){
            document.getElementById('formkategori').style.display='block';
            document.getElementById('formperiode').style.display='block';
            document.getElementById('formwaktu').style.display='block';
            document.getElementById('formcabang').style.display='block';
            document.getElementById('formcenter').style.display='block';
            
            document.getElementById('formjenistema').style.display='none';
            document.getElementById('formtema').style.display='none';
            document.getElementById('formketerangan').style.display='none';
            document.getElementById('formanggota').style.display='none';
            document.getElementById('formjmlpeserta').style.display='block';
            document.getElementById('formuploadlampiran').style.display='block';
            document.getElementById('formuploadfoto').style.display='block';
            document.getElementById('savedatalaporan').disabled =false;
            document.getElementById('formanggotapt').style.display='none';
            document.getElementById('formanggotajp').style.display='none';
            document.getElementById('formanggotapk').style.display='none';
            document.getElementById('formanggotaer').style.display='none';         

        }else if(kode=="A0003"){
            document.getElementById('formkategori').style.display='block';
            document.getElementById('formperiode').style.display='block';
            document.getElementById('formwaktu').style.display='block';
            document.getElementById('formcabang').style.display='block';
            document.getElementById('formcenter').style.display='none';
            document.getElementById('formjenistema').style.display='block';
            document.getElementById('formtema').style.display='block';
            document.getElementById('formketerangan').style.display='none';
            document.getElementById('formanggota').style.display='none';
            document.getElementById('formjmlpeserta').style.display='block';
            document.getElementById('formuploadlampiran').style.display='block';
            document.getElementById('formuploadfoto').style.display='block';
            document.getElementById('savedatalaporan').disabled =false;
            document.getElementById('formanggotapt').style.display='none';
            document.getElementById('formanggotajp').style.display='none';
            document.getElementById('formanggotapk').style.display='none';
            document.getElementById('formanggotaer').style.display='none';         

        }else if(kode=="A0004"){
            // alert('test');
            document.getElementById('formkategori').style.display='block';
            document.getElementById('formperiode').style.display='block';
            document.getElementById('formwaktu').style.display='block';
            document.getElementById('formcabang').style.display='block';
            document.getElementById('formcenter').style.display='block';
            document.getElementById('formjenistema').style.display='none';
            document.getElementById('formtema').style.display='none';
            document.getElementById('formketerangan').style.display='none';
            document.getElementById('formanggota').style.display='block';
            document.getElementById('formjmlpeserta').style.display='none';
            document.getElementById('formuploadlampiran').style.display='none';
            document.getElementById('formuploadfoto').style.display='block';
            document.getElementById('savedatalaporan').disabled =false;
            document.getElementById('formanggotapt').style.display='none';
            document.getElementById('formanggotajp').style.display='none';
            document.getElementById('formanggotapk').style.display='none';
            document.getElementById('formanggotaer').style.display='none';
        }else if(kode=="A0005"){
            document.getElementById('formkategori').style.display='block';
            document.getElementById('formperiode').style.display='block';
            document.getElementById('formwaktu').style.display='block';
            document.getElementById('formcabang').style.display='none';
            document.getElementById('formcenter').style.display='none';
            document.getElementById('formtema').style.display='none';
            document.getElementById('formjenistema').style.display='none';
            document.getElementById('formketerangan').style.display='none';
            document.getElementById('formanggota').style.display='none';
            document.getElementById('formjmlpeserta').style.display='none';
            document.getElementById('formuploadlampiran').style.display='block';
            document.getElementById('formuploadfoto').style.display='block';
            document.getElementById('savedatalaporan').disabled =false; 
            document.getElementById('formanggotapt').style.display='none';
            document.getElementById('formanggotajp').style.display='none';
            document.getElementById('formanggotapk').style.display='none';
            document.getElementById('formanggotaer').style.display='none';
        }
        else if(kode=="DEFAULT"){
            document.getElementById('formkategori').style.display='block';
            document.getElementById('formperiode').style.display='none';
            document.getElementById('formwaktu').style.display='none';
            document.getElementById('formcabang').style.display='none';
            document.getElementById('formcenter').style.display='none';
            document.getElementById('formtema').style.display='none';
            
            document.getElementById('formjenistema').style.display='none';
            document.getElementById('formketerangan').style.display='none';
            document.getElementById('formanggota').style.display='none';
            document.getElementById('formjmlpeserta').style.display='none';
            document.getElementById('formuploadlampiran').style.display='none';
            document.getElementById('formuploadfoto').style.display='none'; 
            document.getElementById('savedatalaporan').disabled =true; 
            document.getElementById('formanggotapt').style.display='none';
            document.getElementById('formanggotajp').style.display='none';
            document.getElementById('formanggotapk').style.display='none';
            document.getElementById('formanggotaer').style.display='none';
        }
        else{
            document.getElementById('formkategori').style.display='block';
            document.getElementById('formperiode').style.display='none';
            document.getElementById('formwaktu').style.display='none';
            document.getElementById('formcabang').style.display='none';
            document.getElementById('formcenter').style.display='none';
            document.getElementById('formtema').style.display='none';
            document.getElementById('formjenistema').style.display='none';
            document.getElementById('formketerangan').style.display='none';
            document.getElementById('formanggota').style.display='none';
            document.getElementById('formjmlpeserta').style.display='none';
            document.getElementById('formuploadlampiran').style.display='none';
            document.getElementById('formuploadfoto').style.display='none'; 
            document.getElementById('savedatalaporan').disabled =true; 
            document.getElementById('formanggotapt').style.display='none';
            document.getElementById('formanggotajp').style.display='none';
            document.getElementById('formanggotapk').style.display='none';
            document.getElementById('formanggotaer').style.display='none';
        }
    }
    $('#kode_kategori').on('change', function() {
        
        var kode_kategori = $("#kode_kategori").val();
        hideForm(kode_kategori);
        
    });

    $('#anggota').on('change', function() {
        
        var kode_kategori = $("#kode_kategori").val();
        var cabang = $("#list_cabang").val();
        var center = $("#center").val();
        var anggota = $("#anggota").val();
        $("#loadingpt").show();
        $("#loadingjp").show();
        $("#loadingpk").show();
        $("#loadingper").show();
        if(kode_kategori=="A0004"){
            if(cabang!=""){
                $.ajax(
                {
                    type: "POST",
                    url: 'action.php?act=load_data_info_anggota',
                    data: {
                        cabang: cabang,
                        center:center,
                        anggota:anggota
                    },
                    beforeSend: function(e) {
                        if(e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) {
                        // alert('test');
                        $("#loadingpt").hide();
                        $("#loadingjp").hide();
                        $("#loadingpk").hide();
                        $("#loadingper").hide();
                        $("#pt").html(response.name).show();
                        $("#pk").html(response.ke).show();
                        $("#per").html(response.period).show();
                        $("#jp").html(response.loam).show();
                        // $("#hp") .html(response.phone).show();  
                        // $("#de") .html(response.dusun).show();  
                        // $("#kab").html(response.kab).show();  
                        // $("#reg").html(response.area).show();  
                        // $("#tgb").html(response.tgl).show();
                        // $("#loadcenter").html(load).hide();
                        // $("#center").html(response.data_center).show();
                        // if(kode_kategori=="A0004"){

                        //     changeclient(cabang);
                        // }
                    }
                });
            }
        }
        
    });
    $('#list_cabang').on('change', function() {
        
        var kode_kategori = $("#kode_kategori").val();
        var cabang = $("#list_cabang").val();
        // alert(kode_kategori);
        $("#showformcenter").hide();
        // document.getElementById('formcenter').style.display='none';
        if(kode_kategori=="A0002" || kode_kategori=="A0004"){
            if(cabang!=""){
                
                var load = "<img src='img/loading.gif' width='18'> <small>Loading...</small>";
                // document.getElementById('formcenter').style.display='block';
                $("#loadcenter").html(load).show();
                $.ajax(
                {
                    type: "POST",
                    url: 'action.php?act=load_data_center_cabang',
                    data: {
                        cabang: cabang
                    },
                    beforeSend: function(e) {
                        if(e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) {
                        // alert('test');
                        $("#loadcenter").html(load).hide();
                        $("#center").html(response.data_center).show();
                        // if(kode_kategori=="A0004"){

                        //     changeclient(cabang);
                        // }
                    }
                });
                $("#loadcenter").html(load).hide();
            }
        }
        // hideForm(kode_kategori);
        
    });
    function changeclient(cabang){
        var kode_kategori = $("#kode_kategori").val();
        var cabang = $("#list_cabang").val();
        var center = $("#center").val();
        $("#showformanggota").hide();
        // document.getElementById('formanggota').style.display='none';
        if(kode_kategori=="A0004"){
            console.log(cabang);
            if(cabang!=""){
                
                var load = "<img src='img/loading.gif' width='18'> <small>Loading...</small>";
                // document.getElementById('formanggota').style.display='block';
                $("#loadanggota").html(load).show();
            $.ajax(
                {
                    type: "POST",
                    url: 'action.php?act=load_data_anggota',
                    data: {
                        cabang: cabang,
                        center:center
                    },
                    beforeSend: function(e) {
                        if(e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) {
                        // alert('test');
                        $("#loadanggota").html(load).hide();
                        $("#anggota").html(response.data_anggota).show();
                    }
                });
                $("#loadanggota").html(load).hide();
            }
        }
    }
    
    $('#center').on('change', function() {
        // alert('test');
        var kode_kategori = $("#kode_kategori").val();
        var cabang = $("#list_cabang").val();
        var center = $("#center").val();
        // alert(kode_kategori);
        $("#showformanggota").hide();
        // document.getElementById('formanggota').style.display='none';
        if(kode_kategori=="A0004"){
            console.log(cabang);
            if(cabang!=""){
                
                var load = "<img src='img/loading.gif' width='18'> <small>Loading...</small>";
                // document.getElementById('formanggota').style.display='block';
                $("#loadanggota").html(load).show();
            $.ajax(
                {
                    type: "POST",
                    url: 'action.php?act=load_data_anggota',
                    data: {
                        cabang: cabang,
                        center:center
                    },
                    beforeSend: function(e) {
                        if(e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) {
                        // alert('test');
                        $("#loadanggota").html(load).hide();
                        $("#anggota").html(response.data_anggota).show();
                    }
                });
                $("#loadanggota").html(load).hide();
            }
        }
        // hideForm(kode_kategori);
        
    });
    $('#datepickeraa').on('change', function() {
        var pilihbulan = $('#datepickeraa').val();
        document.getElementById('showtablelaporan').style.display='none';
        // alert(pilihbulan);
        if(pilihbulan)
        {
            
            document.getElementById('showtablelaporan').style.display='block';
            var post_bulan = pilihbulan;
            var tablelaporanbulanan = $('#tablelaporanbulanan').dataTable({
                destroy: true,
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5'
                ],
                columnDefs: [{
                        targets: [0,1,2,9,13,14,15],
                        className: 'dt-body-center'
                    }
                
                ],
                paging: true,
                "pageLength": 10,
                searching: false,
                ordering: true,
                // ajax: "action.php?act=load_laporan_bulanan",
                ajax: {
                    "url": "action.php?act=load_laporan_bulanan",
                    "type": "POST",
                    "data": {
                        data:post_bulan
                    }
                 },
                columns: [{
                "data": "no"
                },
                {
                "data": "tgl_input"
                },
                {
                "data": "periode_dari"
                },
                {
                "data": "periode_dari"
                },
                {
                "data": "waktu_dari"
                },
                {
                "data": "waktu_dari"
                },
                {
                "data": "kode_kategori"
                },
                {
                "data": "kategori"
                },
                {
                "data": "jenis_tema"
                },
                {
                "data": "nama_jenis_tema"
                },
                {
                "data": "cabang"
                },
                {
                "data": "nama"
                },
        
                {
                "data": "keterangan"
                },
                {
                "data": "pic"
                },
                {
                "data": "jml_peserta"
                },
                {
                "data": "user"
                },
                
                {
                "data": "status"
                }
                ],
        
            });
            tablelaporanbulanan.clear();
            tablelaporanbulanan.reload();
        }

    });
    $('#savedatajenis').on('click', function() {
        var tipe = $('#tipe').val();
        var kode_kategori = $('#kode_kategori').val();
        var nama_kategori = $('#nama_kategori').val();
        // var status = $('#status').val();
        var ele = document.getElementsByName('example-radios');
        var status="";
        for(i = 0; i < ele.length; i++) {
            if(ele[i].checked)
            status = ele[i].value;
        }
        if(nama_kategori=="" || nama_kategori==null)
        {
            swal({
                        title: "Data Konfirmasi !",
                        text: 'Nama kategori harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
        }
        else{
            swal({
            title: "Anda Yakin Menyimpan Data ini ?",
            icon: "warning",
            buttons: ["Batal", "Simpan"]
            })
            .then((willSave) => {
                if(willSave)
                {
                    $.ajax({
                        type: "POST",  
                        url: "action.php?act=savedatakategori",  
                        data: {
                            tipe:tipe,
                            kode_kategori:kode_kategori,
                            nama_kategori:nama_kategori,
                            status:status
                        }, // data yang akan dikirim ke file yang dituju
                        dataType: "json",
                        beforeSend: function(e) {
                            if(e && e.overrideMimeType) {
                                e.overrideMimeType("application/json;charset=UTF-8");
                            }
                        },
                        success: function(response){  
                               if ( response.hasil == "oke") {
                                
                                    swal("Good job!", "Data Berhasil di Simpan",  {
                                        icon: "success",
                                        type: "success"
                                                                
                                        }).then(function() {
                                                // $("#hasilprodsavbranch").html(response.hasil).show();
                                                // document.location('content.php?menu=tablet');
                                                window.location.href = "content.php?menu=jenistema";
                                        }); 
                                } 
                        },
                        error: function (response) { // Ketika ada error
                            // swal("Ada Masalah di Database!", "Silahkan Proses Kembali " , {   icon: "error", buttons: "OK",});
                            // $("#buttonprodsavbranch").show();
                            // $("#loadprodsavbranch").hide();
                        }
                    });
                }
                // else{

                //     alert('batal');
                // }
            });
        }

    });
    $('#savedata').on('click', function() {
        var tipe = $('#tipe').val();
        var kode_kategori = $('#kode_kategori').val();
        var nama_kategori = $('#nama_kategori').val();
        // var status = $('#status').val();
        var ele = document.getElementsByName('example-radios');
        var status="";
        for(i = 0; i < ele.length; i++) {
            if(ele[i].checked)
            status = ele[i].value;
        }
        if(nama_kategori=="" || nama_kategori==null)
        {
            swal({
                        title: "Data Konfirmasi !",
                        text: 'Nama kategori harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
        }
        else{
            swal({
            title: "Anda Yakin Menyimpan Data ini ?",
            icon: "warning",
            buttons: ["Batal", "Simpan"]
            })
            .then((willSave) => {
                if(willSave)
                {
                    $.ajax({
                        type: "POST",  
                        url: "action.php?act=savedatakategori",  
                        data: {
                            tipe:tipe,
                            kode_kategori:kode_kategori,
                            nama_kategori:nama_kategori,
                            status:status
                        }, // data yang akan dikirim ke file yang dituju
                        dataType: "json",
                        beforeSend: function(e) {
                            if(e && e.overrideMimeType) {
                                e.overrideMimeType("application/json;charset=UTF-8");
                            }
                        },
                        success: function(response){  
                               if ( response.hasil == "oke") {
                                
                                    swal("Good job!", "Data Berhasil di Simpan",  {
                                        icon: "success",
                                        type: "success"
                                                                
                                        }).then(function() {
                                                // $("#hasilprodsavbranch").html(response.hasil).show();
                                                // document.location('content.php?menu=tablet');
                                                window.location.href = "content.php?menu=kategori";
                                        }); 
                                } 
                        },
                        error: function (response) { // Ketika ada error
                            // swal("Ada Masalah di Database!", "Silahkan Proses Kembali " , {   icon: "error", buttons: "OK",});
                            // $("#buttonprodsavbranch").show();
                            // $("#loadprodsavbranch").hide();
                        }
                    });
                }
                // else{

                //     alert('batal');
                // }
            });
        }

    });
    $('#savedatalaporan').on('click', function() {
        
        var id_laporan = $("#id_laporan").val();
        var kode_kategori = $("#kode_kategori").val();
        var kode_jenis = $("#kode_jenis").val();
        // alert(kode_kategori);
        var tgl_dari = $("#tgl_dari").val();
        // alert(tgl_dari);
        var tgl_sampai = $("#tgl_sampai").val();
        // alert(tgl_sampai);
        var waktu_dari = $("#waktu_dari").val();
        // alert(waktu_dari);
        var waktu_sampai = $("#waktu_sampai").val();
        // alert(waktu_sampai);
        var cabang = $("#list_cabang").val();
        // alert(cabang);
        var tema = $("#judul").val();
        // alert(tema);
        var keterangan = CKEDITOR.instances.keterangan.getData();
        // alert keterangan);
        var center = $("#center").val();
        // alert(center);
        var anggota = $("#anggota").val();
        // alert(anggota);
        var jml_peserta = $("#jml_peserta").val();
        // alert(jml_peserta);
        var upload_lampiran = $("#upload_lampiran").val();
        // alert(upload_lampiran);
        var upload_foto = $("#upload_foto").val();

        var image_before = $("#image_before").val();
        // alert(upload_lampiran);
        var file_before = $("#file_before").val();
        // alert(file_before);
        // alert(id_laporan);
        // alert(upload_foto);
        var flag ="gagal";
        if(id_laporan!="")
        {
            if(kode_kategori=="A0001"){
                if(kode_kategori=="" || kode_kategori==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Kegiatan harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(tgl_dari=="" || tgl_dari==undefined || tgl_sampai=="" || tgl_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Periode harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(waktu_dari=="" || waktu_dari==undefined || waktu_sampai=="" || waktu_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Waktu  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });

                } else
                if(cabang=="" || cabang==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Cabang  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                
                if(kode_jenis=="" || kode_jenis==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Tema  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(tema=="" || tema==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Tema  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(keterangan=="" || keterangan==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Keterangan  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(jml_peserta=="" || jml_peserta==0){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jumlah Peserta harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(upload_lampiran=="" && file_before==""){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload File harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_foto=="" && image_before=="" ){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload Foto harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } 
                else{
                    flag="ok";
                }
            }
            else  if(kode_kategori=="A0002"){
                if(kode_kategori=="" || kode_kategori==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Kegiatan harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(tgl_dari=="" || tgl_dari==undefined || tgl_sampai=="" || tgl_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Periode harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(waktu_dari=="" || waktu_dari==undefined || waktu_sampai=="" || waktu_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Waktu  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });

                } else
                if(cabang=="" || cabang==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Cabang  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(center=="" || center==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Center  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(jml_peserta=="" || jml_peserta==0){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jumlah Peserta harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(upload_lampiran=="" && file_before==""){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload File harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_foto=="" && image_before=="" ){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload Foto harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } 
                else{
                    flag="ok";
                }
            }
            else  if(kode_kategori=="A0003"){
                if(kode_kategori=="" || kode_kategori==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Kegiatan harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(tgl_dari=="" || tgl_dari==undefined || tgl_sampai=="" || tgl_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Periode harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(waktu_dari=="" || waktu_dari==undefined || waktu_sampai=="" || waktu_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Waktu  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });

                } else
                if(cabang=="" || cabang==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Cabang  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(kode_jenis=="" || kode_jenis==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Tema  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(tema=="" || tema==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Judul  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(jml_peserta=="" || jml_peserta==0){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jumlah Peserta harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(upload_lampiran=="" && file_before==""){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload File harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_foto=="" && image_before=="" ){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload Foto harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } 
                else{
                    flag="ok";
                }
            }
            else  if(kode_kategori=="A0004"){
                if(kode_kategori=="" || kode_kategori==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Kegiatan harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(tgl_dari=="" || tgl_dari==undefined || tgl_sampai=="" || tgl_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Periode harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(waktu_dari=="" || waktu_dari==undefined || waktu_sampai=="" || waktu_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Waktu  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });

                } else
                if(cabang=="" || cabang==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Cabang  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(center=="" || center==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Center  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(anggota=="" || anggota==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Anggota  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_foto=="" && image_before=="" ){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload Foto harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } 
                else{
                    flag="ok";
                }
            }
            else  if(kode_kategori=="A0005"){
                if(kode_kategori=="" || kode_kategori==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Kegiatan harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(tgl_dari=="" || tgl_dari==undefined || tgl_sampai=="" || tgl_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Periode harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(waktu_dari=="" || waktu_dari==undefined || waktu_sampai=="" || waktu_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Waktu  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });

                } else
                if(upload_lampiran=="" && file_before==""){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload File harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_foto=="" && image_before=="" ){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload Foto harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } 
                else{
                    flag="ok";
                }
            }
        }
        else
        {


            if(kode_kategori=="A0001"){
                if(kode_kategori=="" || kode_kategori==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Kegiatan harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(tgl_dari=="" || tgl_dari==undefined || tgl_sampai=="" || tgl_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Periode harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(waktu_dari=="" || waktu_dari==undefined || waktu_sampai=="" || waktu_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Waktu  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });

                } else
                if(cabang=="" || cabang==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Cabang  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(kode_jenis=="" || kode_jenis==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Tema  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(tema=="" || tema==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Tema  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(keterangan=="" || keterangan==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Keterangan  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(jml_peserta=="" || jml_peserta==0){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jumlah Peserta harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(upload_lampiran=="" || upload_lampiran==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload File harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_foto=="" || upload_foto==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload Foto harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } 
                else{
                    flag="ok";
                }
            }
            else  if(kode_kategori=="A0002"){
                if(kode_kategori=="" || kode_kategori==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Kegiatan harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(tgl_dari=="" || tgl_dari==undefined || tgl_sampai=="" || tgl_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Periode harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(waktu_dari=="" || waktu_dari==undefined || waktu_sampai=="" || waktu_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Waktu  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });

                } else
                if(cabang=="" || cabang==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Cabang  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(center=="" || center==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Center  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_lampiran=="" || upload_lampiran==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload File harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_foto=="" || upload_foto==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload Foto harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } 
                else{
                    flag="ok";
                }
            }
            else  if(kode_kategori=="A0003"){
                if(kode_kategori=="" || kode_kategori==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Kegiatan harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(tgl_dari=="" || tgl_dari==undefined || tgl_sampai=="" || tgl_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Periode harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(waktu_dari=="" || waktu_dari==undefined || waktu_sampai=="" || waktu_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Waktu  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });

                } else
                if(cabang=="" || cabang==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Cabang  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(kode_jenis=="" || kode_jenis==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Tema  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(tema=="" || tema==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Judul  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(jml_peserta=="" || jml_peserta==0){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jumlah Peserta harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(upload_lampiran=="" || upload_lampiran==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload File harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_foto=="" || upload_foto==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload Foto harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } 
                else{
                    flag="ok";
                }
            }
            else  if(kode_kategori=="A0004"){
                if(kode_kategori=="" || kode_kategori==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Kegiatan harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(tgl_dari=="" || tgl_dari==undefined || tgl_sampai=="" || tgl_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Periode harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(waktu_dari=="" || waktu_dari==undefined || waktu_sampai=="" || waktu_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Waktu  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });

                } else
                if(cabang=="" || cabang==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Cabang  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(center=="" || center==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Center  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(anggota=="" || anggota==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Anggota  harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_foto=="" || upload_foto==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload Foto harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } 
                else{
                    flag="ok";
                }
            }
            else  if(kode_kategori=="A0005"){
                if(kode_kategori=="" || kode_kategori==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Jenis Kegiatan harus di Pilih',
                        icon: "warning",
                        button: "OK!",
                    });
                } else 
                if(tgl_dari=="" || tgl_dari==undefined || tgl_sampai=="" || tgl_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Periode harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(waktu_dari=="" || waktu_dari==undefined || waktu_sampai=="" || waktu_sampai==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Waktu  harus di Isi',
                        icon: "warning",
                        button: "OK!",
                    });

                } else
                if(upload_lampiran=="" || upload_lampiran==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload File harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } else
                if(upload_foto=="" || upload_foto==undefined){
                    swal({
                        title: "Data Konfirmasi !",
                        text: 'Upload Foto harus di Input',
                        icon: "warning",
                        button: "OK!",
                    });
                } 
                else{
                    flag="ok";
                }
            }
        }

        if(flag=="ok")
        {
            swal({
                title: "Anda Yakin Menyimpan Data ini ?",
                icon: "warning",
                buttons: ["Batal", "Simpan"]
                })
                .then((willSave) => {
                    if(willSave)
                    {
                        // var fd = new FormData();
                        // var data = new FormData(); 
                        var frm = $('#formlaporan');
                        var formData = new FormData(frm[0]);
                        formData.append('file', $('input[type=file]')[0].files[0]);
                        // formData.append('file', $('input[type=file]')[0].files[0]);
                        formData.append('keterangan',keterangan);    
                        // alert('test');
                        $.ajax({
                            type: "POST",
                            url: 'action.php?act=savedatalaporan',
                            data:  formData,
                            processData: false,
                            contentType: false,
                            success: function (response) {
                                var jsonData = JSON.parse(response);
        
                                // alert(JsonData.Success);
                                if (jsonData.Success) {
                                    swal({
                                        title: "Data Confirmation!",
                                        text: jsonData.Message,
                                        icon: "success",
                                        button: "OK!",
                                    });
                                    window.location.replace("content.php?menu=laporan");
                                    // document.location("content.php?menu=form_uji_kelayakan&notrx="+jsonData.NoTrx+"")
                                } else {
                                    swal({
                                        title: "Error Data!",
                                        text: jsonData.Message,
                                        icon: "warning",
                                        button: "OK!",
                                    });
                                }
                            }
                        });
                    }
                });
            
            

        }

    });
    $('#dashtablelaporan').dataTable({
        
        initComplete: function () {
            var column = this.api().column(3);


            var select = $('<select class="form-control" ><option value="">ALL</option></select>')
                .appendTo($('#kategori').empty().text('Jenis Kegiatan : '))
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    column
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();

                });


            column.data().unique().sort().each(function (d, j) {
                // alert(column3);
                // console.log(column3);
                // console.log(column3[j]);
                select.append('<option value="' + d + '">' + d + '</option>');
            });
            var column1 = this.api().column(5);


            var select1 = $('<select class="form-control" ><option value="">ALL</option></select>')
                .appendTo($('#tema').empty().text('Jenis Tema : '))
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    column1
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();

                });


            column1.data().unique().sort().each(function (d, j) {
                // alert(column3);
                // console.log(column3);
                // console.log(column3[j]);
                select1.append('<option value="' + d + '">' + d + '</option>');
            });


        },
        columnDefs: [{
                targets: [0,1,2,8,9,10,11,12,13,14,15],
                className: 'dt-body-center'
            }
            ,
            {
                "targets": [3,5,12,13],
                "visible": false
            }
        ],
        paging: true,
        "pageLength": 10,
        searching: true,
        ordering: true,
        ajax: "action.php?act=load_laporan_dashboard",
        columns: [{
        "data": "no"
        },
        {
        "data": "periode"
        },
        {
        "data": "waktu"
        },
        {
        "data": "kode_kategori"
        },
        {
        "data": "kategori"
        },
        {
        "data": "kode_jenis"
        },
        {
        "data": "jenistema"
        },
        {
        "data": "cabang"
        },
        {
        "data": "nama"
        },

        {
        "data": "keterangan"
        },
        {
        "data": "pic"
        },
        {
        "data": "jml_peserta"
        },
        {
        "data": "foto"
        },
        {
        "data": "dokumen"
        },
        {
        "data": "user"
        },
        {
        "data": "status"
        },
        {
        "data": "action"
        }
        ]

    });
    
    $('#tablelaporan').dataTable({
        initComplete: function () {
            var column = this.api().column(4);
            var column1 = this.api().column(6);


            var select = $('<select class="form-control" ><option value="">ALL</option></select>')
                .appendTo($('#kategori').empty().text('Jenis Kegiatan : '))
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    column
                        .search(val ? '^' + val + '$' : '', true, false)
                        .draw();

                });

            var select1 = $('<select class="form-control" ><option value="">ALL</option></select>')
            .appendTo($('#jenis_tema').empty().text('Jenis Tema : '))
            .on('change', function () {
                var val = $.fn.dataTable.util.escapeRegex(
                    $(this).val()
                );
                column1
                    .search(val ? '^' + val + '$' : '', true, false)
                    .draw();

            });


            column.data().unique().sort().each(function (d, j) {

                select.append('<option value="' + d + '">' + d + '</option>');
            });
            column1.data().unique().sort().each(function (d, j) {

                select1.append('<option value="' + d + '">' + d + '</option>');
            });


        },
        columnDefs: [{
                targets: [0,1,2,12,13,14,15,16],
                className: 'dt-body-center'
            }
            ,
            {
                "targets": [3,4,6,11,12,13,14],
                "visible": false
            }
        ],
        paging: true,
        "pageLength": 10,
        searching: true,
        ordering: true,
        ajax: "action.php?act=load_laporan",
        columns: [{
        "data": "no"
        },
        {
        "data": "tgl_input"
        },
        {
        "data": "periode"
        },
        {
        "data": "waktu"
        },
        {
        "data": "kode_kategori"
        },
        {
        "data": "kategori"
        },
        {
        "data": "kode_jenis_tema"
        },
        {
        "data": "nama_jenis_tema"
        },
        {
        "data": "cabang"
        },
        {
        "data": "nama"
        },

        {
        "data": "keterangan"
        },
        {
        "data": "pic"
        },
        {
        "data": "jml_peserta"
        },
        {
        "data": "foto"
        },
        {
        "data": "dokumen"
        },
        {
        "data": "status"
        },
        {
        "data": "action"
        }
        ],

    });
    
    $('#tablekategori').dataTable({
        columnDefs: [{
                targets: [0,1,2,4,5],
                className: 'dt-body-center'
            }
        ],
        
        paging: true,
        "pageLength": 10,
        searching: true,
        ordering: true,
        ajax: "action.php?act=load_kategori",
        columns: [{
        "data": "no"
        },
        {
        "data": "tipe"
        },
        {
        "data": "kode"
        },
        {
        "data": "nama"
        },
        {
        "data": "status"
        },
        {
        "data": "action"
        }

        ]

    });
    $('#tablejenistema').dataTable({
        columnDefs: [{
                targets: [0,1,2,4,5],
                className: 'dt-body-center'
            }
        ],
        
        paging: true,
        "pageLength": 10,
        searching: true,
        ordering: true,
        ajax: "action.php?act=load_jenis_tema",
        columns: [{
        "data": "no"
        },
        {
        "data": "tipe"
        },
        {
        "data": "kode"
        },
        {
        "data": "nama"
        },
        {
        "data": "status"
        },
        {
        "data": "action"
        }

        ]

    });


});