$(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
	// Kita sembunyikan dulu untuk loadingnya
	 
	$("#loadingagt").hide();
	
	 
	
	$("#kelompok").change(function(){ // Ketika user mengganti atau memilih data provinsi
		$("#anggota").hide(); // Sembunyikan dulu combobox kota nya
		$("#loadingagt").show(); // Tampilkan loadingnya
	
		$.ajax({
			type: "POST", // Method pengiriman data bisa dengan GET atau POST
			url: "option_agt.php", // Isi dengan url/path file php yang dituju
			data: {center : $("#center").val(), cabang : $("#cabang").val() , kelompok: $("#kelompok").val()  }, // data yang akan dikirim ke file yang dituju
			dataType: "json",
			beforeSend: function(e) {
				if(e && e.overrideMimeType) {
					e.overrideMimeType("application/json;charset=UTF-8");
				}
			},
			success: function(response){ // Ketika proses pengiriman berhasil
				setTimeout(function(){
					$("#loadingagt").hide(); // Sembunyikan loadingnya

					// set isi dari combobox kota
					// lalu munculkan kembali combobox kotanya
					$("#anggota").html(response.data_agt).show();
				}, 3000);
			},
			error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
				alert(thrownError); // Munculkan alert error
			}
		});
    });
	
	
});
