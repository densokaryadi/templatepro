$(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
	// Kita sembunyikan dulu untuk loadingnya
	 
	$("#loadingagt").hide();
	
	 
	
	$("#prosestombol").click(function(){ // Ketika user mengganti atau memilih data provinsi
		$("#proses").hide(); // Sembunyikan dulu combobox kota nya
		$("#loadingagt").show(); // Tampilkan loadingnya
	
		$.ajax({
			type: "POST", // Method pengiriman data bisa dengan GET atau POST
			url: "proses_wo.php", // Isi dengan url/path file php yang dituju
			data: {user : $("#user").val(), agt : $("#agt").val() , dates: $("#dates").val()  }, // data yang akan dikirim ke file yang dituju
			dataType: "json",
			beforeSend: function(e) {
				if(e && e.overrideMimeType) {
					e.overrideMimeType("application/json;charset=UTF-8");
				}
			},
			success: function(response){ // Ketika proses pengiriman berhasil
				setTimeout(function(){
					$("#loadingagt").hide(); // Sembunyikan loadingnya

					// set isi dari combobox kota
					// lalu munculkan kembali combobox kotanya
					$("#hasil").html(response.data_agt).show();
				}, 3000);
			},
			error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
				alert(thrownError); // Munculkan alert error
			}
		});
    });
	
	
});
