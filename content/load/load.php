<?php
// print_r($_SESSION);
$getListLaporan = $Mdisspmclass->getDataLaporanSPMById("");
// print_r($getListLaporan);
$no		= 1;
$ary 	= array();
$ary2 	= array();
if(is_array($getListLaporan)){
    foreach ($getListLaporan as $idx =>$val){
        $ary['no'] 		= $no;
		$ary['tgl_input'] = $val['tgl_input'];
        $ary['periode']	= $val['tgl_dari'] ." s/d ".$val['tgl_sampai'];
        $ary['waktu']	= $val['jam_dari'] ." s/d ".$val['jam_sampai'];
		$ary['kode_kategori']	= $val['kode'] . " - " .$val['nama'];
		$ary['kategori']	= "<b>".$val['nama']."</b>";
		$ary['kode_jenis_tema']	= $val['jenis_tema'] . " - " .$val['nama_jenis_tema'];
		$ary['nama_jenis_tema']	= "<b>".$val['nama_jenis_tema']."</b>";
		
		$nama_cabang="";
		if($val['cabang']!=""){
			$getInfoCabang = $Mdismoclass->getInfoCabang($val['cabang']);
			$nama_cabang = $getInfoCabang['branchname'];
		}
		$ary['cabang']	= "<b>".$val['cabang']." - ".$nama_cabang."</b>";
		$ary['nama']	= $val['judul'];
		$ary['keterangan']	= html_entity_decode($val['keterangan']);
		$ary['pic']	= $val['anggota'];
		$ary['jml_peserta']	= $val['jml_peserta'];
		$ary['foto']	= $val['foto'];
		$ary['dokumen']	= $val['dokumen'];
		if($val['status']=="1"){ //WAITING APPROVAL
			$name_status = "<span class=\"label label-warning\"><strong>Waiting Approval</strong></span>";
		}
        if($val['status']=="0"){ //APPROVE
			$name_status = "<span class=\"label label-success\"><strong>Approve</strong></span>";
		}
        if($val['status']=="2"){ //REJECT

			$name_status = "<span class=\"label label-danger\"><strong>Reject</strong></span>
                <br>
                <p><b>".$val['alasan']."</b></p>
                ";
		}
		$ary['status']	= $name_status;
		if($_SESSION['levelmo']=="Admin"){
			if(in_array($val['status'],[1,2])){
				$ary['action'] =  "<a href=\"content.php?menu=editdatalaporan&id_lap=".$val['id_laporan']."\" title='Edit Laporan' class=\"btn btn-warning\" id=\"upload\") > <i class='fa fa-pencil'></i></a>";
			}
			else{
				$ary['action']="  <a href=\"content.php?menu=detaildatalaporan&id_lap=".$val['id_laporan']."&key=lap\"  title='Cek Detail' class=\"btn btn-primary\" id=\"detail\") > <i class='fa fa-search'></i></a>";
			}
		}
		else{
			$ary['action']="";
		}
		$ary2[] = $ary; 
        $no++;
    }
}
$arr = array('data' => $ary2 );
echo json_encode($arr);